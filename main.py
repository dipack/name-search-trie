import os
import sys
import time
from trie import Trie

def setup():
    args = sys.argv

    phone_book = Trie()

    filename = "names.txt"
    search_val = "Jul"

    if len(args) >= 2 and args[1]:
        tmp_filename = args[1]
        if os.path.isfile(tmp_filename):
            print("Using {}".format(tmp_filename))
            filename = tmp_filename
    if len(args) >= 3 and args[2]:
        search_val = args[2]

    lines = []
    with open(filename, 'r') as f:
        lines = f.readlines()
    for name in lines:
        cleaned = name.strip('\n').strip('\t').casefold()
        phone_book.grow_leaves(cleaned)

    return phone_book, lines, search_val

def brute_force_search(lines, search_val):
    found = []
    for name in lines:
        if name.startswith(search_val):
            found.append(name)
    return found

def run():
    phone_book, lines, search_val = setup()
    start_time = time.time()
    stop_time = time.time()
    if phone_book.check_value(search_val.casefold()):
        stop_time = time.time()
        print("{0} exists in our phone book!".format(search_val))
    else:
        stop_time = time.time()
        print("{0} does not exist in our phone book!".format(search_val))
    trie_delta = stop_time - start_time
    print("Trie search took", trie_delta)
    start_time = time.time()
    found = brute_force_search(lines, search_val)
    stop_time = time.time()
    brute_force_delta = stop_time - start_time
    print("Brute force search took", brute_force_delta)
    return

if __name__ == "__main__":
    run()

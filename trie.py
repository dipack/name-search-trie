class Node():
    def __init__(self):
        self.children = {}
        self.is_leaf = False

class Trie():
    def __init__(self, value: str=""):
        self.value = value
        self.children = {}
        return

    def grow_leaves(self, value: str):
        splits = list(value.casefold())
        if len(splits):
            first = splits[0]
            if not first in self.children:
                if len(splits) > 1:
                    sub_tree = Trie(first)
                    self.children[first] = sub_tree.grow_leaves("".join(splits[1:]))
                elif len(splits) == 1:
                    if self.value != "":
                        self.value = first
                    self.children[first] = Trie(first)
                    return self
            else:
                self.children.get(first).grow_leaves("".join(splits[1:]))
        return self

    def check_value(self, value: str) -> bool:
        children = self.children
        splits = list(value)
        first = splits[0]
        if children and first in children:
            sub_node = children[first]
            # If leaf node
            if not sub_node.children:
                if len(splits) == 2:
                    second = splits[1]
                    return sub_node.value == second
            # In this case, the original search term was a suffix
            if len(splits) == 1:
                sub_node.print_tree(True)
                return True
            # Go down the children to continue search
            if len(splits) > 1:
                return sub_node.check_value("".join(splits[1:]))
        else:
            print("No children", first, splits, self.value)
            if len(splits) == 1:
                return self.value == first
        return False

    def print_tree(self, explanation=True):
        children = self.children
        if children:
            for val, child in children.items():
                if explanation:
                    print("Printing Sub tree of {0}:".format(val))
                child.print_tree(explanation)
        else:
            if explanation:
                print("Leaf node: {0}".format(self.value))
            return self.value
        return ""
